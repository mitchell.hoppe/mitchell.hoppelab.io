 // function to set a given theme/color-scheme
function setTheme(themeName) {
    localStorage.setItem('theme', themeName);
    document.documentElement.className = themeName;
}

// function to toggle between light and dark theme
function toggleTheme() {
    if (localStorage.getItem('theme') === 'theme-dark') {
        setTheme('theme-light');
    } else {
        setTheme('theme-dark');
    }
}

// Immediately invoked function to set the theme on initial load
(function () {
    if (localStorage.getItem('theme') === 'theme-dark') {
        setTheme('theme-dark');
        document.getElementById('slider').checked = false;
    } else {
        setTheme('theme-light');
        document.getElementById('slider').checked = true;
    }
})();

window.addEventListener("load", () => {
  document.querySelector("body").classList.add("loaded");
 });

 
 function valid_name() {
   return document.getElementById('name').value.length > 0;
  }
  
  function valid_email() {
    var email = document.getElementById('email').value;
    var mailformat = /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/;
    if(email.match(mailformat))
    {
      return validate_temp(email);
    }
    return false;
  }
  
document.getElementById('message').value = "";
function valid_message() {
  return document.getElementById('message').value.length > 0 &&
        document.getElementById('message').value.length < 300;
}

function send_message() {
  var validName = valid_name();
  var validEmail = valid_email();
  var validMessage = valid_message();
  if(
    validName &&
    validEmail && 
    validMessage
  ){
    var name = document.getElementById('name').value;
    var email = document.getElementById('email').value;
    var message = document.getElementById('message').value;
    Email.send({
      SecureToken : "545b3277-29fb-49d3-8e52-10a61555a1b7",
      To : 'mitchell.o.hoppe@gmail.com',
      From : 'mitchell.o.hoppe@gmail.com',
      Subject : name + '( ' + email + ' )',
      Body : message
    });
  } else {
    if(!validName) {document.getElementById('name').classList.add('error');}
    if(!validEmail) {document.getElementById('email').classList.add('error');}
    if(!validMessage) {document.getElementById('message').classList.add('error');}
    throw 'Filled out form not valid';
  }
}

function click_button(div, callback){
  new Promise((resolve, reject) => {
    setTimeout( function() {
      try {
        callback();
        resolve();
        document.querySelector('#send').consumed = true;
        document.querySelector('#send').disabled = true;
        document.querySelector('#btn-progress').style.opacity = 0;
      } catch (error) {
        reject(error);
      }
    }, 0);
  });
}

function updateButton(e) {
  var count = 0;
  var validName = valid_name();
  var validEmail = valid_email();
  var validMessage = valid_message();
  count += (validName ? 1 : 0) + (validEmail ? 1 : 0) + (validMessage ? 1 : 0);

  if(!document.querySelector('#send').consumed){
    document.querySelector('#send').disabled = (count != 3);
  }

  document.querySelector('#btn-progress').style.width = count * 33.333 + '%';
}

function createRipple(event) {
  const button = event.currentTarget;

  const circle = document.createElement("span");
  const diameter = Math.max(button.clientWidth, button.clientHeight);
  const radius = diameter / 2;

  circle.style.width = circle.style.height = `${diameter}px`;
  circle.style.left = `${event.clientX - button.offsetLeft - radius}px`;
  circle.style.top = `${event.clientY + button.offsetTop + radius}px`;

  circle.classList.add("ripple");

  const ripple = button.getElementsByClassName("ripple")[0];

  if (ripple) {
    ripple.remove();
  }

  document.getElementsByClassName("wrapper")[0].appendChild(circle);
}

const buttons = document.getElementsByTagName("button");
for (const button of buttons) {
  button.addEventListener("click", createRipple);
}