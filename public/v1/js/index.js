
const debounce = (fn) => {

    // This holds the requestAnimationFrame reference, so we can cancel it if we wish
    let frame;
  
    // The debounce function returns a new function that can receive a variable number of arguments
    return (...params) => {
      
      // If the frame variable has been defined, clear it now, and queue for next frame
      if (frame) { 
        cancelAnimationFrame(frame);
      }
  
      // Queue our function call for the next frame
      frame = requestAnimationFrame(() => {
        
        // Call our function and pass any params we received
        fn(...params);
      });
  
    } 
  };
  
  
  // Reads out the scroll position and stores it in the data attribute
  // so we can use it in our stylesheets
  const storeScroll = () => {
    document.documentElement.dataset.scroll = window.scrollY;
  }
  
  // Listen for new scroll events, here we debounce our `storeScroll` function
  document.addEventListener('scroll', debounce(storeScroll), { passive: true });
  
  // Update scroll position for first time
  storeScroll();

document.getElementById('message').value = "";

function valid_name() {
  return document.getElementById('name').value.length > 0;
}

function valid_email() {
  var email = document.getElementById('email').value;
  var mailformat = /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/;
  if(email.match(mailformat))
  {
    return validate_temp(email);
  }
  return false;
}

function valid_message() {
  return document.getElementById('message').value.length > 0 &&
        document.getElementById('message').value.length < 1000;
}

function send_message() {
  var validName = valid_name();
  var validEmail = valid_email();
  var validMessage = valid_message();
  if(
    validName &&
    validEmail && 
    validMessage
  ){
    var name = document.getElementById('name').value;
    var email = document.getElementById('email').value;
    var message = document.getElementById('message').value;
    Email.send({
      SecureToken : "545b3277-29fb-49d3-8e52-10a61555a1b7",
      To : 'mitchell.o.hoppe@gmail.com',
      From : 'mitchell.o.hoppe@gmail.com',
      Subject : name + '( ' + email + ' )',
      Body : message
    });
  } else {
    if(!validName) {document.getElementById('name').classList.add('error');}
    if(!validEmail) {document.getElementById('email').classList.add('error');}
    if(!validMessage) {document.getElementById('message').classList.add('error');}
    throw 'Filled out form not valid';
  }
}

function click_button(div, callback){
  new Promise((resolve, reject) => {
    div.disabled = true;
    var threeDots = div.getElementsByClassName('dot-three');
    for (i = 0; i < threeDots.length; i++) {
      threeDots[i].style.opacity = 1;
      threeDots[i].classList.add('dot-flashing');
    }
    
    setTimeout( function() {
      try {
        callback();
        resolve();
      } catch (error) {
        reject(error);
      } finally {
        var threeDots = div.getElementsByClassName('dot-three');
        for (i = 0; i < threeDots.length; i++) {
          threeDots[i].style.opacity = 0;
          threeDots[i].classList.remove('dot-flashing');
        }
      }
    }, 0) 
  }).then(() => {
    var check = div.getElementsByClassName('check');
    for (i = 0; i < check.length; i++) {
      var paths = check[i].getElementsByClassName('path');
      for (j = 0; j < paths.length; j++) {
        paths[j].style.opacity = 1;
        paths[j].classList.add('draw');
      }
    }
  }).then(() => {
    if(div.dataset.loop == undefined || div.dataset.loop == 'true' || div.dataset.loop != 'false')
    {
      setTimeout( function() {
        div.disabled = false;
        var check = div.getElementsByClassName('check');
        for (i = 0; i < check.length; i++) {
          var paths = check[i].getElementsByClassName('path');
          for (j = 0; j < paths.length; j++) {
            paths[j].style.opacity = 0;
            paths[j].classList.remove('draw');
          }
        }
      }, 1200);
    }
  }).catch(function(e) {
    var cross = div.getElementsByClassName('cross');
    for (i = 0; i < cross.length; i++) {
      var paths = cross[i].getElementsByClassName('path');
      for (j = 0; j < paths.length; j++) {
        paths[j].style.opacity = 1;
        paths[j].classList.add('draw');
      }
    }
    setTimeout( function() {
      div.disabled = false;
      var cross = div.getElementsByClassName('cross');
      for (i = 0; i < cross.length; i++) {
        var paths = cross[i].getElementsByClassName('path');
        for (j = 0; j < paths.length; j++) {
          paths[j].style.opacity = 0;
          paths[j].classList.remove('draw');
        }
      }
    }, 1200)
  });
}