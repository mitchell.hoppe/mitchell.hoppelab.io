(function($) {
    $.fn.hasScrollBar = function() {
        return (this.get(0).scrollHeight > this.height());
    }
})(jQuery);

$(function() {
    $(document).on("click", function (event) {
        var clickover = $(event.target);
        var $navbar = $(".navbar-collapse");
        if (!clickover.hasClass('nav-link')) {
            $navbar.collapse('hide');
        }
    });
  });

$(document).ready(function(){
    function update_char_count(){
        var characterCount = $('#message').val().length, 
            theCount = $('#char-count');

        if (characterCount > 1000){
            $('#message').val('');
        }
        if (characterCount > 500 && characterCount <= 1000){
            theCount.text(1000 - characterCount);
        } else{
            theCount.text('');
        }
    }       
    update_char_count();

    $('#name').on('input change', function(){
        update_char_count();
    });

    $('#name').on('input change', function(){
        $('#name').removeClass('error');
    });
    $('#email').on('input change', function(){
        $('#email').removeClass('error');
    });
    $('#message').on('input change', function(){
        $('#message').removeClass('error');
    });
});