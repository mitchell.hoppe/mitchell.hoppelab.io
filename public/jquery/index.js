$(document).on('mousemove mousewheel', function(e) {
    var offset = $('#back-pane').offset();
    $("#mouse-grad").css({left:e.pageX - offset.left, top:e.pageY - offset.top});
});

$(window).on('load', function() { 
    $("body").removeClass("preload");
 });


//Movement Animation to happen
const card = document.querySelector(".resume-link");
const container = document.querySelector(".resume-wrapper");
//Items
const header = document.querySelector(".resume-link h5");
const lines = document.querySelectorAll(".pseudo-line");

//Moving Animation Event
container.addEventListener("mousemove", (e) => {
    var offset = $('.resume-link').offset();
    let xAxis = (card.clientWidth / 2 - (e.pageX - offset.left)) / 5;
    let yAxis = (-card.clientHeight / 2 + (e.pageY - offset.top)) / 5;
    card.style.transform = `rotateY(${xAxis}deg) rotateX(${yAxis}deg) scale(1.05)`;
});
//Animate In
container.addEventListener("mouseenter", (e) => {
    card.style.transition = "none";
    card.style.perspective = "1000px";
    //Popout
    header.style.transform = "translateZ(25.1px)";
    lines.forEach(function (line) {
        line.style.transform = "translateZ(25.1px)";
      });
});
//Animate Out
container.addEventListener("mouseleave", (e) => {
    card.style.transition = "all 0.5s ease";
    card.style.transform = `rotateY(0deg) rotateX(0deg) scale(1)`;
    //Popback
    header.style.transform = "translateZ(0px)";
    lines.forEach(function (line) {
        line.style.transform = "translateZ(0px)";
    });
});

$('#contact input').each(function (index) {
    $( this ).on("change paste keyup", updateButton);
});
$('#contact textarea').each(function (index) {
    $( this ).on("change paste keyup", updateButton);
});